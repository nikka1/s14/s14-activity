console.log("Hello World"); 

let firstName = 'John';
let lastName = 'Smith';
let age = 30;

let firstNameField = "First Name: " + firstName;
console.log(firstNameField);

let lastNameField = "Last Name: " + lastName;
console.log(lastNameField);

let ageField = "Age: " + age;
console.log(ageField);

let hobbiesField = "Hobbies:";
console.log(hobbiesField);

let hobbiesList = ["Biking", "Mountain Climbing","Swimming"];
console.log(hobbiesList);

let workAddressField = "Work Address:";
console.log(workAddressField);

let fullWorkAddress = {
	houseNumber: "32",
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska"
}
console.log(fullWorkAddress)

let note = "This was printed inside of the function"

function printUserInfo(firstName, lastName, age) {
	console.log(firstName + " " + lastName + " " + "is" + " " + age + " " + "years of age.")
}

printUserInfo(firstName, lastName, age);
console.log(note);

function printOtherInfo(hobbiesList) {
	console.log(hobbiesList)
}

printOtherInfo(hobbiesList);
console.log(note);

function printMoreInfo(fullWorkAddress) {
	console.log(fullWorkAddress)
}

printMoreInfo(fullWorkAddress);